import logging
from enum import Enum

from mypy_boto3_ec2.service_resource import Instance

logger = logging.getLogger(__name__)


class State(Enum):
    PENDING = 'pending'
    RUNNING = 'running'
    SHUTTINGDOWN = "shutting-down"
    TERMINATED = 'terminated'
    STOPPING = 'stopping'
    STOPPED = 'stopped'


class EC2Instance(object):

    def __init__(self, ec2_instance: Instance):
        self.start_timeout = 30
        self.ec2_instance = ec2_instance

    def _state(self) -> State:
        self.ec2_instance.load()
        return State(self.ec2_instance.state['Name'])

    def stop(self):
        if self._state() != State.STOPPED:
            if self._state() != State.STOPPING:
                logger.info('Stopping instance')
                self.ec2_instance.stop()
            self.ec2_instance.wait_until_stopped()
        else:
            logger.info('Instance already stopped')
        logger.info('Instance Stopped')
        return

    def start(self):

        if self._state() != State.RUNNING:
            if self._state() != State.STOPPED:
                logger.info('Wait until Stopped')
                self.ec2_instance.wait_until_stopped()

            logger.info('Starting instance')
            self.ec2_instance.start()
            self.ec2_instance.wait_until_running()
        else:
            logger.info('Instance is already running')

        logger.info(f'Instance is running at ip {self.get_publick_dns_name()}')

    def get_publick_dns_name(self):
        self.ec2_instance.load()
        return self.ec2_instance.public_dns_name
