from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    AWS_ACCESS_KEY_ID: str
    AWS_SECRET_ACCESS_KEY: str
    AWS_REGION_NAME: str = 'eu-west-1'
    INSTANCE_ID: str

    model_config = SettingsConfigDict(env_file='.env')


config = Settings()
