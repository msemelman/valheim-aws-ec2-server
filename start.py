import logging

from factory import build_instance

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    valheim_instance = build_instance()
    valheim_instance.start()


if __name__ == '__main__':
    main()
