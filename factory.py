import boto3

from config import config
from ec2_instance import EC2Instance


def build_instance():
    session = boto3.Session(aws_access_key_id=config.AWS_ACCESS_KEY_ID,
                            aws_secret_access_key=config.AWS_SECRET_ACCESS_KEY,
                            region_name=config.AWS_REGION_NAME)
    ec2 = session.resource('ec2')
    valheim_instance: EC2Instance = EC2Instance(ec2.Instance(config.INSTANCE_ID))
    return valheim_instance
